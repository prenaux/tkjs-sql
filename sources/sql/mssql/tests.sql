---
--- See http://stackoverflow.com/a/2885664 for the rational of why we use IF -> CREATE, ALTER PROCEDURE.
---

IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE type = 'P' AND name = 'tests_list')
  EXEC ('CREATE PROCEDURE [dbo].[tests_list] AS BEGIN SET NOCOUNT ON; END')
GO
-- !{Export}
ALTER PROCEDURE tests_list(@Limit INT)
AS
BEGIN
  SELECT TOP (@Limit)
    device_id,
    desc_txt,
    status_txt
  FROM tests
  WHERE drivers_avail_req_sid = 'z8fD45';
END;
GO

IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE type = 'P' AND name = 'tests_list_and_count')
  EXEC ('CREATE PROCEDURE [dbo].[tests_list_and_count] AS BEGIN SET NOCOUNT ON; END')
GO
-- !{Export}
ALTER PROCEDURE tests_list_and_count(@Limit INT)
AS
BEGIN
  SELECT TOP (@Limit) COUNT(*) AS num_test_entries
  FROM tests;
  SELECT
    device_id,
    desc_txt,
    status_txt
  FROM tests
  WHERE drivers_avail_req_sid = 'z8fD45';
END;
GO

IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE type = 'P' AND name = 'tests_set_status')
  EXEC ('CREATE PROCEDURE [dbo].[tests_set_status] AS BEGIN SET NOCOUNT ON; END')
GO
-- !{Export}
ALTER PROCEDURE tests_set_status(@DeviceId  INT,
                                 @NewStatus VARCHAR(16)) -- multiline on purpose to test the parser...
AS
BEGIN
  UPDATE tests
  SET status_txt = @NewStatus
  WHERE drivers_avail_req_sid = 'z8fD45' AND device_id = @DeviceId;
  -- By convention a SP should return the number of row it has changed with its sp's name as key
  SELECT @@ROWCOUNT AS tests_set_status;
END;
GO

IF NOT EXISTS(SELECT *
              FROM sys.objects
              WHERE type = 'P' AND name = 'tests_get_one')
  EXEC ('CREATE PROCEDURE [dbo].[tests_get_one] AS BEGIN SET NOCOUNT ON; END')
GO
-- !{Export}
ALTER PROCEDURE tests_get_one(@DeviceId INT)
AS
BEGIN
  SELECT *
  FROM tests
  WHERE drivers_avail_req_sid = 'z8fD45' AND device_id = @DeviceId;
END;
GO
