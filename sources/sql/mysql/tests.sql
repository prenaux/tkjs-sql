DROP PROCEDURE IF EXISTS tests_list;
-- !{Export}
CREATE PROCEDURE tests_list(aLimit INT)
  BEGIN
    SELECT
      device_id,
      desc_txt,
      status_txt
    FROM tests
    WHERE drivers_avail_req_sid = 'z8fD45'
    LIMIT aLimit;
  END;

DROP PROCEDURE IF EXISTS tests_list_and_count;
-- !{Export}
CREATE PROCEDURE tests_list_and_count(aLimit INT)
  BEGIN
    SELECT COUNT(*) AS num_test_entries
    FROM tests;
    SELECT
      device_id,
      desc_txt,
      status_txt
    FROM tests
    WHERE drivers_avail_req_sid = 'z8fD45'
    LIMIT aLimit;
  END;

DROP PROCEDURE IF EXISTS tests_set_status;
-- !{Export}
CREATE PROCEDURE tests_set_status(aDeviceId INT, aNewStatus TEXT)
  BEGIN
    UPDATE tests
    SET status_txt = aNewStatus
    WHERE drivers_avail_req_sid = 'z8fD45' AND device_id = aDeviceId;
  END;

DROP PROCEDURE IF EXISTS tests_get_one;
-- !{Export}
CREATE PROCEDURE tests_get_one(aDeviceId INT)
  BEGIN
    SELECT *
    FROM tests
    WHERE drivers_avail_req_sid = 'z8fD45' AND device_id = aDeviceId;
  END;
